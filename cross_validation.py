from functools import reduce

import numpy as np

from io_utils import read_csv

def stratified_folds(D, k):
    X, Y = D
    classes = np.unique(Y)

    folds = []
    for i in range(k):
        fold = []
        for c in classes:
            indexes = np.where(Y[:] == c)
            Yj = Y[indexes]

            i0 = int(i * Yj.shape[0] / k)
            i1 = int((i+1) * Yj.shape[0] / k)

            fold += list(indexes[0][i0:i1])
            
        folds.append(fold)

    assert sum(map(len, folds)) == X.shape[0]
    return folds


def stratified_cross_validation(D, k):
    """
    Returns a list of Training Datasets
        where a Training Dataset is a pair of Datasets (D_train, D_test)
    """
    X, Y = D
    folds = stratified_folds(D, k)
    Ds = []

    for i in range(len(folds)):
        test_fold = folds[i]
        train_fold = reduce(lambda x,y: x+y, map(lambda x: folds[x], filter(lambda x: x!=i, range(len(folds)))))
        
        assert all(map(lambda x: x not in train_fold, test_fold))
        
        Ds.append((
            (X[train_fold,:], Y[train_fold]),
            (X[test_fold,:], Y[test_fold])
        ))
    return Ds
  
def inspect_dataset_folds(D, k):
    TDs = stratified_cross_validation(D, 5)

    print(D[0].shape, D[1].shape)
    for i, (D_train, D_test) in enumerate(TDs):
        print("%d-th fold" % (i+1))
        print(D_train[0].shape, D_train[1].shape)
        print(D_test[0].shape, D_test[1].shape)
  
def main():
    datasets_files = [
        ("data/dadosBenchmark_validacaoAlgoritmoAD_types.csv", ';'),
        ("data/dataset_31_credit-g.csv", ','),
        ("data/dataset_191_wine.csv", ','),
        ("data/phpOkU53r.csv", ',')
    ]
    k = 10

    for (f,sep) in datasets_files:
        print("\nInspecting %s" % (f,))
        D, *_ = read_csv(f, encode_categoricals=False, sep_char=sep)
        inspect_dataset_folds(D, k)
    

main() if __name__ == '__main__' else True