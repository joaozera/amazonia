import os

import numpy as np

from evaluate import recall, precision, fmeasure

def main():
    for csv_filename in map(lambda x: os.path.join("predictions", x), os.listdir("predictions")):
        fold_predictions = {}
        positive_classes = set()
        with open(csv_filename) as f:
            lines = map(lambda x: x.strip(), f.readlines()[1:])
            for line in lines:
                fold, y_true, y_pred = map(int, line.split(','))
                
                if fold not in fold_predictions:
                    fold_predictions[fold] = [], []
                fold_predictions[fold][0].append(y_true)
                fold_predictions[fold][1].append(y_pred)

                positive_classes.add(y_true)

        metrics_per_class = {}
        for positive_class in positive_classes:
            recalls = []
            precisions = []
            fs = []
            for (real_classes, predictions) in fold_predictions.values():
                r = recall(real_classes, predictions, positive_class)
                p = precision(real_classes, predictions, positive_class)
                f = fmeasure(p, r)

                recalls.append(r)
                precisions.append(p)
                fs.append(f)
            metrics_per_class[positive_class] = {
                'recall': (np.mean(recalls), np.std(recalls)),
                'precision': (np.mean(precisions), np.std(precisions)),
                'f-measure': (np.mean(fs), np.std(fs))
            }
        print(metrics_per_class)

        output_filename = os.path.join("metrics_results", "results_"+os.path.basename(csv_filename))
        with open(output_filename, 'w') as f:
            f.write("class,recall-mean,recall-std,precision-mean,precision-std,f1-measure-mean,f1-measure-std\n")
            for c,m in metrics_per_class.items():
                f.write("%d,%f,%f,%f,%f,%f,%f\n" % (c, *m['recall'], *m['precision'], *m['f-measure']))


main() if __name__ == '__main__' else True