import numpy as np

from bootstrap import bootstrap
from decision_tree import DecisionTree
from io_utils import read_csv

class RandomForest():
    def __init__(self, D, n_trees, n_attributes, data_types):
        self.trees = []
        self.arg_indexes = []
        self.data_types = data_types
        self.attr_vals = [np.unique(D[0][:,attr_id]) for attr_id in range(D[0].shape[1])]

        print("Initializing Random Forest with %d trees..."%(n_trees,), end=' ')
        for _ in range(n_trees):
            D_train, _ = bootstrap(D)
            X, Y = D_train
            N, M = X.shape

            indexes = np.random.choice(M, n_attributes, replace=False)

            tree = DecisionTree((np.take(X, indexes, axis=1), Y), np.take(data_types, indexes), np.take(self.attr_vals, indexes))

            self.trees.append(tree)
            self.arg_indexes.append(indexes)
        print("Done.")

    def majority_vote(self, X):
        votes = []

        for tree, indexes in zip(self.trees, self.arg_indexes):
            votes.append(tree.predict(np.take(X, indexes)))

        predictions, num_votes = np.unique(votes, return_counts=True)

        votes=np.array(votes)

        return predictions[np.argmax(num_votes)]

def main():
    # N = 50
    # M = 10
    # Kx = 10
    # Ky = 10
    #
    # X = np.random.randint(1, Kx, (N,M))
    # Y = np.random.randint(1, Ky, N)
    # D = X, Y
    benchmark_file = "data/dadosBenchmark_validacaoAlgoritmoAD_types.csv"
    D, header, data_types = read_csv(benchmark_file, encode_categoricals=False)

    X, Y = D
    N, M = X.shape

    n_attributes = int(np.floor(np.sqrt(M)))
    forest = RandomForest(D, 100, n_attributes, data_types)

    dummy_input = np.array(['Ensolarado', 'Quente', 'Alta', 'Falso'])
    dummy_output = forest.majority_vote(dummy_input)
    print(dummy_input, dummy_output)
    assert dummy_output == 'Nao'

main() if __name__ == '__main__' else True
