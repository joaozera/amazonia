import random

import numpy as np
import networkx as nx

from io_utils import read_csv
from information_gain import information_gains

class DecisionTree():
    def __init__(self, D, attr_types, attrs_vals, attributes_to_sample=None, verbose_induction=False):
        self.verbose_induction = verbose_induction
        
        self.X, self.Y = D
        self.N, self.M = self.X.shape

        self.attr_types = attr_types
        self.attrs_vals = attrs_vals

        self.attributes_to_sample = self.M if attributes_to_sample is None or attributes_to_sample > self.M or attributes_to_sample < 0 else attributes_to_sample

        ''' tree induction '''
        self._tree_induction_()

    def _is_type_categorical_(self, attr):
        t = self.attr_types[attr]
        assert t in ('cat', 'num')
        return t == 'cat'

    def _induce_node_(self, Dj, L):
        """ 
            returns 
                attribute with best information gain if there is such (-1 otherwise),
                data set partitions for each of its possible values and
                attribute mean value if it is numeric (None otherwise)
        """
        if len(L) == 0:
            return -1, Dj, None

        gains = information_gains(Dj, self.attrs_vals)
        
        ''' turns *some* attributes null upon argmax selection '''
        attrs_to_burn = max(0, (len(L) - self.attributes_to_sample))
        for i in random.sample(L, attrs_to_burn):
            gains[i] = -1
        for i in filter(lambda x: x not in L, range(self.M)):
            gains[i] = -1

        best_attr = np.argmax(gains)
        max_gain = np.max(gains)
        is_type_categorical = self._is_type_categorical_(best_attr)

        if max_gain > 0:
            X, Y = Dj    
            partitions = {}
            
            if is_type_categorical:
                attr_vals = self.attrs_vals[best_attr]
                attr_val_mean = None
            else:
                attr_vals = np.array([1, 2])
                attr_val_mean = np.mean(X[:, best_attr])
            for j in attr_vals:
                if is_type_categorical:
                    j_indices = np.where(X[:, best_attr] == j)
                else:
                    
                    if j == 1:
                        j_indices = np.where(X[:, best_attr] < attr_val_mean)
                    else:
                        assert j==2
                        j_indices = np.where(X[:, best_attr] >= attr_val_mean)
                
                Dj = X[j_indices], Y[j_indices[0]]
                partitions[j] = Dj
                
            assert sum(map(lambda p: p[0].shape[0], partitions.values())) == X.shape[0]
            if self.verbose_induction:
                print("Best gain: %f, from %d" % (max_gain, best_attr))
            return best_attr, partitions, attr_val_mean
        else:
            return -1, Dj, None

    def _induce_tree_(self, parent_node, remaining_attrs, parent_value, parent_mean, partition):
        best_attr, partitions, attr_mean = self._induce_node_(partition, remaining_attrs)
        # print("Induced attribute %d from parent %s" % (best_attr, parent_node))

        if best_attr >= 0 :
            remaining_attrs.remove(best_attr)
            for attr_val, partition in partitions.items():
                if self.verbose_induction:
                    print("Edge (%d->%d)" % (parent_node, best_attr))
                self.tree.add_edge(parent_node, best_attr, split_value=parent_value, attr_mean=parent_mean)
                self._induce_tree_(best_attr, remaining_attrs, attr_val, attr_mean, partition)
        else:
            ''' leaf node '''
            if self.verbose_induction:
                print("Leaf node")
            self.tree.add_edge(parent_node, "L"+str(len(self.tree.nodes)), split_value=parent_value, attr_mean=parent_mean)
            # print(partition[1]) # labels distribution

    def _tree_induction_(self):
        self.tree = nx.DiGraph()

        remaining_attrs = set(range(self.M))
        best_attr, partitions, attr_mean = self._induce_node_((self.X, self.Y), remaining_attrs)
        
        if best_attr >= 0:
            remaining_attrs.remove(best_attr)

            self.tree.add_node(best_attr)
            self.tree_root_attr = best_attr
            for attr_val, partition in partitions.items():
                self._induce_tree_(best_attr, remaining_attrs, attr_val, attr_mean, partition)
        else:
            ''' leaf-root node '''          
            self.tree.add_node("L0")
            self.tree_root_attr = "L0"
        
    def _prediction_recursion_(self, inp, D, node):
        X, Y = D
        
        match = False
        outcoming_edges = list(filter(lambda d: d[0] == node, self.tree.edges().data()))
        is_type_categorical = self._is_type_categorical_(node) if len(outcoming_edges) > 0 else None
        for e in outcoming_edges:
            if self.attr_types[node] == 'cat' and inp[node] == e[2]['split_value']:
                match = True
                attr_indices = np.where(X[:, node] == e[2]['split_value'])
            elif self.attr_types[node] == 'num':
                match = True
                if inp[node] < e[2]['attr_mean']:
                    attr_indices = np.where(X[:, node] < e[2]['attr_mean'])
                else:
                    attr_indices = np.where(X[:, node] >= e[2]['attr_mean'])
            if match:
                Dj = X[attr_indices], Y[attr_indices[0]]
                return self._prediction_recursion_(inp, Dj, e[1])
        
        if len(outcoming_edges) > 0:
            raise ValueError("Non-leaf node but none of its edges split values match")
        else:
            #print("Stopping at node %s" % (node,))
            unique, counts = np.unique(Y, return_counts=True)
            if len(counts) > 0:
                return unique[np.argmax(counts)]
            else:
                return random.sample(list(self.Y[0]), 1)[0]

    def predict(self, X):
        try:
            if len(X.shape) == 2:
                assert X.shape[1] == self.M and X.shape[0] == 1
            elif len(X.shape) == 1:
                assert X.shape[0] == self.M
            else:
                raise AssertionError()
        except AssertionError:
            raise ValueError("Input should have shape like ({0},) or (1,{0}). It has shape {1} instead".format(self.M, X.shape))
        
        return self._prediction_recursion_(X, (np.copy(self.X), np.copy(self.Y)), self.tree_root_attr)


def main():
    benchmark_file = "data/dadosBenchmark_validacaoAlgoritmoAD_types.csv"
    D, header, data_types = read_csv(benchmark_file, encode_categoricals=False)
    attrs_vals = [np.unique(D[0][:,attr_id]) for attr_id in range(D[0].shape[1])]
    dt = DecisionTree(D, data_types, attrs_vals, verbose_induction=True)

    dummy_input = np.array(['Ensolarado', 'Quente', 'Alta', 'Falso'])
    dummy_output = dt.predict(dummy_input)
    print(dummy_input, dummy_output)
    assert dummy_output == 'Nao'

main() if __name__ == '__main__' else True