import numpy as np

def bootstrap(D):
    X, Y = D
    N, _ = X.shape

    indices = []
    for _ in range(N):
        indices.append(np.random.randint(0,N))
    
    left_out = list(set(range(N)).difference(set(indices)))

    X_b_train = X[indices]
    Y_b_train = Y[indices]
    X_b_test = X[left_out]
    Y_b_test = Y[left_out]

    #print(X_b_train.shape, Y_b_train.shape)
    #print(X_b_test.shape, Y_b_test.shape)

    return (X_b_train, Y_b_train), (X_b_test, Y_b_test)

def main():
    N = 10
    X = np.array(range(N)).reshape(N, 1)
    Y = np.array(map(lambda x: 2*x, range(N)))
    print(X,Y)

    D_train, D_test = bootstrap((X,Y))
    print(D_train)
    print(D_test)

main() if __name__ == '__main__' else True
