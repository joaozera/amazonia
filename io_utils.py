import numpy as np

def read_csv(filepath, header=True, data_types=True, encode_categoricals=True, sep_char=';'):
    ''' only categorical data '''
    X = []
    Y = []

    with open(filepath) as f:
        lines = f.read().splitlines()
        data_line = 0
        
        if header:
            header = lines[0].split(sep_char)
            data_line += 1
        else:
            header = None
        if data_types:
            data_types = list(map(lambda s: s.lower(), lines[1].split(sep_char)))
            data_line += 1
        else:
            data_types = None
        
        for line in map(lambda x: x.strip(), lines[data_line:]):
            s = line.split(sep_char)
            X.append(s[:-1])
            Y.append(s[-1:])

    X = np.array(X)
    Y = np.array(Y)

    N, M = X.shape

    if encode_categoricals:
        for j in range(M):
            if data_types and data_types[j] == 'cat':
                b,c = np.unique(X[:,j], return_inverse=True)
                X[:,j] = c+1

        b,c = np.unique(Y, return_inverse=True)
        Y[:,0] = c+1

        X = X.astype(float)
        
    D = X, Y
    return D, header, data_types
