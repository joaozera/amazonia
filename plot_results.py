from matplotlib import pyplot as plt

summary_csv = 'metrics_results/summary.csv'

lines = []
with open(summary_csv) as f:
    csv_lines = f.readlines()
    for line in csv_lines[1:]:
        s = line.split(',')
        p = tuple(map(float, s[3].split('±')))
        r = tuple(map(float, s[4].split('±')))
        f = tuple(map(float, s[5].split('±')))
        lines.append({
            'dataset': s[0],
            'n_trees': int(s[1]),
            'class': s[2],
            'p_mean': p[0],
            'p_std': p[1],
            'r_mean': r[0],
            'r_std': r[1],
            'f_mean': f[0],
            'f_std': f[1]
        })

graphs_data = {}
for line in lines:
    k = "%s-%s" % (line['dataset'], line['class'])
    if k not in graphs_data:
        graphs_data[k] = {}

    graphs_data[k][line['n_trees']] = {
        'precision': (line['p_mean'], line['p_std']),
        'recall': (line['r_mean'], line['r_std']),
        'f1': (line['f_mean'], line['f_std']),
    }

for k, v in graphs_data.items():
    fig, (ax1) = plt.subplots(1,1)
    plt.ylim(0, 1)
    X = v.keys()
    
    P = list(map(lambda x: v[x]['precision'][0], X))
    P_err = list(map(lambda x: 0.1*v[x]['precision'][1], X))

    R = list(map(lambda x: v[x]['recall'][0], X))
    R_err = list(map(lambda x: 0.1*v[x]['recall'][1], X))

    F = list(map(lambda x: v[x]['f1'][0], X))
    F_err = list(map(lambda x: 0.1*v[x]['f1'][1], X))
    
    ax1.errorbar(X, P, P_err, color='blue')
    ax1.errorbar(X, R, R_err, color='green')
    ax1.errorbar(X, F, F_err, color='red')

    ax1.set_title(k)
    ax1.legend(["Precision", "Recall", "F1"])
    fig.savefig("plots/"+"%s.png"%(k,))
