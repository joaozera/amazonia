import os

import argparse
import numpy as np

from io_utils import read_csv
from cross_validation import stratified_cross_validation
from random_forests import RandomForest

def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("dataset_filepath")
    parser.add_argument("num_trees", type=int)
    parser.add_argument("--num_folds", type=int, default=10)
    parser.add_argument("--dataset_sep_char", default=",")

    return vars(parser.parse_args())

def experience(dataset_filepath, dataset_sep_char, num_folds, num_trees):
    D, header, data_types = read_csv(dataset_filepath, sep_char=dataset_sep_char)

    predictions_filepath = "predictions_%d_%d_"%(num_trees, num_folds) + os.path.basename(dataset_filepath)
    
    content = ""
    content += "fold_id,ground-truth,prediction\n" # csv header
    
    for i, (D_train, D_test) in enumerate(stratified_cross_validation(D, num_folds)):
        X_train, Y_train = D_train
        forest = RandomForest(D_train, num_trees, int(np.floor(np.sqrt(X_train.shape[1]))), data_types)

        for (sample_x, sample_y) in zip(*D_test):
            y_pred = forest.majority_vote(sample_x)

            sample_y = sample_y[0]
            content += "%s,%s,%s\n" % (i, sample_y, y_pred)

    with open(predictions_filepath, 'w') as f:
        f.write(content)

def main():
    args = get_args()

    experience(args["dataset_filepath"], args["dataset_sep_char"], args["num_folds"], args["num_trees"])

main() if __name__ == '__main__' else True