import math

import numpy as np


def dataset_info(D):
    X, Y = D
    N, M = X.shape

    info = 0
    unique_attr_vals, unique_attr_counts = np.unique(Y, return_counts=True)

    for attr, attr_count in zip(unique_attr_vals, unique_attr_counts):
        p = attr_count / float(N)
        info -= p * math.log(p,2)
    return(info)

def attribute_info(D, attr_id, attr_vals):
    X, Y = D
    N, M = X.shape

    assert attr_id < X.shape[1]
    unique_attr_vals, unique_attr_counts = np.unique(X[:,attr_id], return_counts=True)

    info = 0
    for j, count in zip(unique_attr_vals, unique_attr_counts):
        count += 1
        X_j = X[np.where(X[:,attr_id] == j)]
        Y_j = Y[np.where(X[:,attr_id]==j)[0]]

        info += count * dataset_info((X_j, Y_j)) / float(N + len(attr_vals))

    for j in filter(lambda x: x not in unique_attr_vals, attr_vals):
        count = 1
        X_j = X[np.where(X[:,attr_id] == j)]
        Y_j = Y[np.where(X[:,attr_id]==j)[0]]

        info += count * dataset_info((X_j, Y_j)) / float(N + len(attr_vals)) 
    return info

def information_gains(D, attrs_vals):
    X, Y = D
    N, M = X.shape

    d_info = dataset_info(D)

    gains = []
    for i in range(M):
        #print("Apendou um ganho")
        gains.append(d_info - attribute_info(D, i, attrs_vals[i]))
    return gains

def main():
    N = 500
    M = 10
    Kx = 10
    Ky = 10

    X = np.random.randint(1, Kx, (N,M))
    Y = np.random.randint(1, Ky, N)
    D = X, Y

    attrs_vals = [np.unique(X[:,attr_id]) for attr_id in range(M)]

    gains = information_gains(D, attrs_vals)
    print(gains, np.argmax(gains))

main() if __name__ == '__main__' else True
