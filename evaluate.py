import numpy as np

from bootstrap import bootstrap
from decision_tree import DecisionTree
from io_utils import read_csv


def recall(predictions, real_classes, positive_class):
    true_positives = 0
    false_negatives = 0

    for prediction, real_class in zip(predictions, real_classes):
        if real_class == positive_class:
            if prediction == positive_class:
                true_positives += 1
            else:
                false_negatives += 1

    if true_positives > 0:
        return float(true_positives)/(true_positives + false_negatives)
    else:
        return 0

def precision(predictions, real_classes, positive_class):
    true_positives = 0
    false_positives = 0

    for prediction, real_class in zip(predictions, real_classes):
        if prediction == positive_class:
            if real_class == positive_class:
                true_positives += 1
            else:
                false_positives += 1

    if true_positives > 0:
        return float(true_positives)/(true_positives + false_positives)
    else:
        return 0

def fmeasure(precision, recall, beta=1):
    if precision == 0.0 and recall == 0.0:
        return 0.0
    else:
        return (1 + beta**2) * (precision * recall)/((beta**2 * precision) + recall)

def inspect_measures(predictions, real_classes):
    print(predictions)
    print(real_classes)

    rec = recall(predictions, real_classes, 1)
    prec = precision(predictions, real_classes, 1)
    print(rec, prec)
    print(fmeasure(prec, rec))

def main():
    predictions = np.random.randint(1, 3, 100)
    real_classes = np.random.randint(1, 3, 100)
    inspect_measures(predictions, real_classes)    

    predictions = np.array([1] * 100)
    real_classes = np.array([1] * 100)
    inspect_measures(predictions, real_classes)

    predictions = np.array([1] * 100)
    real_classes = np.array([2] * 100)
    inspect_measures(predictions, real_classes)

main() if __name__ == '__main__' else True
