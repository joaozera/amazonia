#!/bin/bash

for N in 10 100 250; do
    for i in $(ls data | grep -v 'Benchmark'); do
        echo $i $N;
        python experiment.py data/$i $N;
    done
done
